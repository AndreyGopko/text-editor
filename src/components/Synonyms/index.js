import React, { Component } from 'react';
import './styles.css';

export default class Synonyms extends Component {
    render() {
        const { synonymsList } = this.props;

        if (!synonymsList || !synonymsList.length) return null;

        return (
            <div className='synonyms-container'>
                <ul className='synonyms-list'>
                    {synonymsList.map((elem, index) => {
                        return (
                            <li
                                key={index}
                                onClick={() => this.props.onSynonymSelect(elem.word)}
                                className='synonyms-item'
                            >
                                {elem.word}
                            </li>
                        )
                    })}
                </ul>
            </div>
        );
    }
}
