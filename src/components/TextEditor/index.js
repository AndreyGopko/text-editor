import React, { Component } from 'react';
import ControlPanel from '../ControlPanel';
import FileZone from '../FileZone';
import { getTagsMap } from '../../utils/helpers';
import './styles.css';

export default class TextEditor extends Component {
    state = {
        tagsMap: {}
    }

    onCheckWord = () => {
        const selection = document.getSelection();
        const anchor = selection && selection.anchorNode;
        let parentNode = anchor.parentNode;
        let tags = [];
    
        while (parentNode.tagName !== 'DIV') {
            const tagName = parentNode.tagName;
            tags.push(tagName);
            parentNode = parentNode.parentNode;
        }

        const tagsMap = getTagsMap(tags);
        this.setState({ tagsMap });
    }

    render() {
        return (
            <div className='text-editor'>
                <ControlPanel
                    tagsMap={this.state.tagsMap}
                    onChange={this.onCheckWord}
                />
                <FileZone
                    textContent={this.props.textContent}
                    onCheckWord={this.onCheckWord}
                    onSelectWord={this.props.onSelectWord}
                />
            </div>
        );
    }
}
