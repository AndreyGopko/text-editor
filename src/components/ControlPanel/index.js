import React, { Component } from 'react';
import cx from 'classnames';
import { applyTextFormat } from '../../utils/helpers';
import {
    BOLD,
    ITALIC,
    UNDERLINE,
} from '../../constants';
import './styles.css';

class ControlPanel extends Component {
    makeBold = () => {
        applyTextFormat(BOLD);
        this.props.onChange()
    }

    makeItalic = () => {
        applyTextFormat(ITALIC);
        this.props.onChange()
    }

    makeUnderline = () => {
        applyTextFormat(UNDERLINE);
        this.props.onChange()
    }

    render() {
        const { tagsMap } = this.props;
        return (
            <div id='control-panel'>
                <div id='format-actions'>
                    <button
                        className={cx('format-action', {
                            'active': tagsMap['B'],
                        })}
                        type='button'
                        onClick={this.makeBold}
                    >
                        <b>B</b>
                    </button>
                    <button
                        className={cx('format-action', {
                            'active': tagsMap['I']
                        })}
                        type='button'
                        onClick={this.makeItalic}
                    >
                        <i>I</i>
                    </button>
                    <button
                        className={cx('format-action', {
                            'active': tagsMap['U']
                        })}
                        type='button'
                        onClick={this.makeUnderline}
                    >
                        <u>U</u>
                    </button>
                </div>
            </div>
        );
    }
}

export default ControlPanel;
