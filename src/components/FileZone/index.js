import React, { Component } from 'react';
import './styles.css';

export default class FileZone extends Component {
    render() {
        return (
            <div className='file-zone'>
                <div
                    id='file'
                    contentEditable
                    suppressContentEditableWarning
                    onClick={this.props.onCheckWord}
                    onKeyUp={this.props.onCheckWord}
                    onDoubleClick={this.props.onSelectWord}
                >
                    {this.props.textContent}
                </div>
            </div>
        );
    }
}
