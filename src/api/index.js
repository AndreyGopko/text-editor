const DATAMUSE_API_URL = 'https://api.datamuse.com';
const DATAMUSE_SYNONYMS_URL = '/words?rel_syn=';

export const getSynonymsByWord = (word) => {
    return fetch(`${DATAMUSE_API_URL}${DATAMUSE_SYNONYMS_URL}${word}`)
        .then(res => res.json())
}