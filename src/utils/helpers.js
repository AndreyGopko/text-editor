export const getMockText = () => {
    return new Promise(function (resolve, reject){
        resolve("A year ago I was in the audience at a gathering of designers in San Francisco. There were four designers on stage, and two of them worked for me. I was there to support them. The topic of design responsibility came up, possibly brought up by one of my designers, I honestly don’t remember the details. What I do remember is that at some point in the discussion I raised my hand and suggested, to this group of designers, that modern design problems were very complex. And we ought to need a license to solve them.");
    })
}

export const saveSelection = () => {
    if (window.getSelection) {
        const sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            const text = sel.toString();
            if (text.endsWith(' ')) {
                sel.modify("extend", "backward", "character");
            }
            return sel.getRangeAt(0);
        }
    } else if (document.selection && document.selection.createRange) {
        return document.selection.createRange();
    }
    return null;
}

export const restoreSelection = (range) => {
    if (range) {
        if (window.getSelection) {
            const sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } else if (document.selection && range.select) {
            range.select();
        }
    }
}

export const applyTextFormat = (type) => {
    document && document.execCommand(type);
}

export const getTagsMap = (tags) => {
    let result = {}
    tags.forEach(elem => {
        result[elem] = true;
    })
    return result;
}