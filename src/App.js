import React, {Component} from 'react';
import TextEditor from './components/TextEditor';
import Synonyms from './components/Synonyms';
import { getMockText, saveSelection, restoreSelection } from './utils/helpers';
import { getSynonymsByWord } from './api';
import './App.css';

class App extends Component {
    state = {
        textContent: '',
        synonymsList: []
    }

    selection = null;

    componentDidMount() {
        this.getText();
    }

    onChange = (event) => {
        this.setState({ textContent: event.target.value });
    }

    getText() {
        getMockText()
            .then((result) => {
                this.setState({ textContent: result });
            });
    }

    onSelectWord = () => {
        const selection = document.getSelection();
        this.selection = saveSelection(); 
        const text = selection.toString().trim();

        getSynonymsByWord(text)
            .then(result => {
                this.setState({ synonymsList: result });
            })
    }

    onSynonymSelect = (text) => {
        restoreSelection(this.selection);
        document.execCommand('insertText', true, text);
        this.setState({ synonymsList: [] });
    }

    render() {
        return (
            <div className='App'>
                <header>
                    <span>Simple Text Editor</span>
                </header>
                <main>
                    <TextEditor
                        textContent={this.state.textContent}
                        onSelectWord={this.onSelectWord}
                    />
                    <Synonyms
                        synonymsList={this.state.synonymsList}
                        onSynonymSelect={this.onSynonymSelect}
                    />
                </main>
            </div>
        );
    }
}

export default App;
